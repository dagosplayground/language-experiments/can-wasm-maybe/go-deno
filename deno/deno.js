import * as _ from "./wasm_exec.js" // Comes from build script in ../go folder
const go = new window.Go();

// Static Go compiled WASM 
const f = await Deno.open("./hdeno.wasm")
const buf = await Deno.readAll(f);
const inst = await WebAssembly.instantiate(buf, go.importObject);
go.run(inst.instance);

// Function in Go compiled WASM (must run with -A flag in Deno)
const whoFile = await Deno.open("./whodeno.wasm");
const whoBuf = await Deno.readAll(whoFile);
const whoInst = await WebAssembly.instantiate(whoBuf, go.importObject);
go.run(whoInst.instance);

// Call function through streaming interface
const whoOutput = whoHello('OSS 2023 Vancouver');
console.log(whoOutput);
console.log(whoHello('everyone watching the replay'))

// Call to Urban Dictionary
const udFile = await Deno.open("./ud.wasm");
const udBuf = await Deno.readAll(udFile);
const udInst = await WebAssembly.instantiate(udBuf, go.importObject);
console.log("\nUrban Dictionary Lookup on Arch linunx:\n")
go.run(udInst.instance);

// Please view https://pkg.go.dev/syscall/js#FuncOf on issues with function calls and http and deadlocks
