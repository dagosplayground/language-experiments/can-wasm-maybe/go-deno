# Deno WASM

The goal of this code base is to run a simple WASM program from a Deno runtime.

## Setup

**Prerequisites:**
* Have go installed and prepared first.
* Have deno installed through your package manager of choice.

1. Go in the `../go` folder first. Build the program by running the `build.sh` script in that folder. All contents needed will be copied to this folder. 
1. Use the `./run.sh` script. 
